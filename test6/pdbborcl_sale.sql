CREATE TABLESPACE sales_tablespace
     DATAFILE 'sales_tablespace.dbf'
     SIZE 100M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   
   CREATE TABLESPACE indexs_tablespace
     DATAFILE 'indexs_tablespace.dbf'
     SIZE 50M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   
   ALTER USER sale_admin DEFAULT TABLESPACE sales_tablespace;
   ALTER USER sale_admin DEFAULT TABLESPACE indexs_tablespace;
   CREATE TABLE PRODUCTS (
        productid   NUMBER PRIMARY KEY,
        productname VARCHAR2(100) NOT NULL,
        price        NUMBER(10, 2) NOT NULL,
        quantity     NUMBER
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE ORDERS (
        orderid    NUMBER PRIMARY KEY,
        orderdate  DATE NOT NULL,
        customerid NUMBER REFERENCES customer(customerid)
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE ORDERTETAILS (
        orderid   NUMBER REFERENCES orders(orderid),
        productid NUMBER REFERENCES products(productid),
        quantity   NUMBER,
        price      NUMBER(10, 2),
        PRIMARY KEY (orderid, productid)
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE CUSTOMER (
        customerid   NUMBER PRIMARY KEY,
        customername VARCHAR2(100) NOT NULL,
        email         VARCHAR2(100)
      )
      TABLESPACE sales_tablespace;
      GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERTETAILS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO sale_admin;
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO products (productid, productname, price, quantity)
       SELECT i AS productid, 'Product ' || i AS productname, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
       FROM dual
       WHERE NOT EXISTS (SELECT 1 FROM products WHERE productid = i);
     
       -- Commit periodically to avoid excessive transaction size
       IF MOD(i, 100) = 0 THEN
         COMMIT;
       END IF;
     END LOOP;
     COMMIT;
   END;
   /
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO customer (customerid, customername, email)
       SELECT i AS customerid, 'Customer ' || i AS customername, 'customer' || i || '@example.com' AS email
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM customer
         WHERE customerid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO orders (orderid, orderdate, customerid)
       SELECT i AS orderid, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS orderdate, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customerid
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM orders
         WHERE orderid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   BEGIN
     FOR i IN 1..30000 LOOP
       MERGE INTO ORDERTETAILS od
       USING (
         SELECT i AS orderid, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS productid, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
         FROM dual
       ) data
       ON (od.orderid = data.orderid AND od.productid = data.productid)
       WHEN NOT MATCHED THEN
         INSERT (orderid, productid, quantity, price)
         VALUES (data.orderid, data.productid, data.quantity, data.price);
     END LOOP;
     COMMIT;
   END;
   /
   CREATE OR REPLACE PACKAGE sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER) IS
       l_order_total NUMBER := 0;
       l_order_date DATE;
       l_product_name VARCHAR2(100);
       l_quantity NUMBER;
       l_price NUMBER(10, 2);
     BEGIN
       
       SELECT SUM(od.quantity * p.price)
       INTO l_order_total
       FROM orders o
       JOIN ordertetails od ON o.orderid = od.orderid
       JOIN products p ON od.productid = p.productid
       WHERE o.orderid = p_orderid;
       
       
       SELECT orderdate
       INTO l_order_date
       FROM orders
       WHERE orderid = p_orderid;
       
       
       DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_order_date, 'MM/DD/YYYY HH24:MI:SS'));
       
       
       FOR r IN (
         SELECT p.productname, od.quantity, od.price
         FROM orders o
         JOIN ordertetails od ON o.orderid = od.orderid
         JOIN products p ON od.productid = p.productid
         WHERE o.orderid = p_orderid
       ) LOOP
         
         DBMS_OUTPUT.PUT_LINE(r.productname || ' (' || r.quantity || ') - $' || r.price);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Order not found');
     END;
   END;
   /
   CREATE OR REPLACE PACKAGE sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER) IS
       l_orderdate DATE;
       l_order_total NUMBER;
     BEGIN
       FOR r IN (
         SELECT o.orderid, o.orderdate
         FROM orders o
         WHERE o.customerid = p_customerid
       ) LOOP
        
         l_orderdate := r.orderdate;
         
         
         SELECT SUM(od.quantity * p.price)
         INTO l_order_total
         FROM ordertetails od
         JOIN products p ON od.productid = p.productid
         WHERE od.orderid = r.orderid;
         
        
         DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_orderdate, 'MM/DD/YYYY HH24:MI:SS'));
         DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Orders not found');
     END;
   END;
   /