# 实验6（期末考核） 基于Oracle数据库的售卖系统
姓名：李沛云
学号：202010414207
班级：软工2班
### 实验目的
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。
### 实验内容
  1. 设计商品销售系统的数据库表
     确定商品销售系统的表单组成，包括订单表、商品表、库存表和客户表，分别用于记录订单信息、商品信息、订单详细信息和客户信息。商品销售系统包括订单生成、发货、退货等业务。
  2. 设计表空间
  设计两个表空间，分别为sales_tablespace和index_tablespace，
  ``` mysql
  CREATE TABLESPACE sales_space
  DATAFILE 'sales_tablespace.dbf'
  SIZE 100M
  NEXT 10M
  MAXSIZE UNLIMITED;

CREATE TABLESPACE indexs_tablespace
  DATAFILE 'indexs_tablespace.dbf'
  SIZE 50M
  NEXT 10M
  MAXSIZE UNLIMITED;

ALTER USER sale_admin DEFAULT TABLESPACE sales_tablespace;
ALTER USER sale_admin DEFAULT TABLESPACE indexs_tablespace; ```
![1](1.png)
3. 创建四个表，首先创建订单表“ORDERS”，包含了订单号、订单生成时间、客户ID、商品。创建商品表“PRODUCT”，包括商品名称、商品编号、价格、数量等。创建客户表”CUSTOMER" 包含了客户姓名、客户ID、客户联系方式等字段。创建”ORDERITEM“表，包含订单ID、商品ID、商品数量、商品价格等字段。然后将订单表和客户表建立关联。
``` mysql
CREATE TABLE PRODUCTS (
  productid   NUMBER PRIMARY KEY,
  productname VARCHAR2(100) NOT NULL,
  price        NUMBER(10, 2) NOT NULL,
  quantity     NUMBER
)
TABLESPACE sales_tablespace;


CREATE TABLE ORDERS (
  orderid    NUMBER PRIMARY KEY,
  orderdate  DATE NOT NULL,
  customerid NUMBER REFERENCES customer(customerid)
)
TABLESPACE sales_tablespace;


CREATE TABLE ORDERITEM (
  orderid   NUMBER REFERENCES orders(orderid),
  productid NUMBER REFERENCES products(productid),
  quantity   NUMBER,
  price      NUMBER(10, 2),
  PRIMARY KEY (orderid, productid)
)
TABLESPACE sales_tablespace;


CREATE TABLE CUSTOMER (
  customerid   NUMBER PRIMARY KEY,
  customername VARCHAR2(100) NOT NULL,
  email         VARCHAR2(100)
)
TABLESPACE sales_tablespace;
```
  ![2](2.1.png)
  ![3](2.2.png)
  ![4](2.3.png)     
4. 对四个表(PRODUCTS、ORDERS、ORDERTETAILS和CUSTOMER)的SELECT、INSERT、UPDATE和DELETE进行授权。
``` mysql
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERS TO sale_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERTETAILS TO sale_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO sale_admin;
```
5. 接下来我们将向"PRODUCTS"表插入虚拟数据，总共插入30,000条记录。
``` mysql
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO products (productid, productname, price, quantity)
    SELECT i AS productid, 'Product ' || i AS productname, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
    FROM dual
    WHERE NOT EXISTS (SELECT 1 FROM products WHERE productid = i);

    -- Commit periodically to avoid excessive transaction size
    IF MOD(i, 100) = 0 THEN
      COMMIT;
    END IF;

  END LOOP;
  COMMIT;
END;
/
```
![5](3.png)
6. 创建和授权用户
创建管理员用户和普通用户。管理员用户拥有对所有表的完全访问权限，而普通用户只有部分访问权限。在授权时要考虑到安全性和灵活性。
``` mysql
CREATE USER sale_ladmin IDENTIFIED BY 123;

GRANT CONNECT, RESOURCE TO sale_ladmin;
CREATE USER sale_luser IDENTIFIED BY 123;
```
![6](4.png)

7. 完成函数和存储过程 
创建了一个名为"sales_lpkg"的包，其中包含了一个名为"print_order_summary"的PROCEDURE程序，输入ID可以通过该程序打印订单总金额、订单日期、订单中每一个商品的名称、数量和单价等信息。
``` mysql
CREATE OR REPLACE PACKAGE sales_lpkg AS
  PROCEDURE print_order_summary(p_orderid IN NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY sales_lpkg AS
  PROCEDURE print_order_summary(p_orderid IN NUMBER) IS
    l_order_total NUMBER := 0;
    l_order_date DATE;
    l_product_name VARCHAR2(100);
    l_quantity NUMBER;
    l_price NUMBER(10, 2);
  BEGIN
    
    SELECT SUM(od.quantity * p.price)
    INTO l_order_total
    FROM orders o
    JOIN ordertetails od ON o.orderid = od.orderid
    JOIN products p ON od.productid = p.productid
    WHERE o.orderid = p_orderid;
    
    
    SELECT orderdate
    INTO l_order_date
    FROM orders
    WHERE orderid = p_orderid;
    
    
    DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
    DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_order_date, 'MM/DD/YYYY HH24:MI:SS'));
    
    
    FOR r IN (
      SELECT p.productname, od.quantity, od.price
      FROM orders o
      JOIN ordertetails od ON o.orderid = od.orderid
      JOIN products p ON od.productid = p.productid
      WHERE o.orderid = p_orderid
    ) LOOP
      
      DBMS_OUTPUT.PUT_LINE(r.productname || ' (' || r.quantity || ') - $' || r.price);
    END LOOP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Order not found');
  END;
END;
/
```
![7](5.png)
8. 完成数据库的备份 
``` mysql
sqlplus / as sysdba

SHUTDOWN IMMEDIATE

STARTUP MOUNT

ALTER DATABASE ARCHIVELOG；

ALTER DATABASE OPEN；
rman target /
SHOW ALL;

BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;

LIST BACKUP;
RUN {
  CONFIGURE CONTROLFILE AUTOBACKUP ON;
  CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup/autobackup/%F';
  CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
  CONFIGURE BACKUP OPTIMIZATION ON;

  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG DELETE INPUT;

  CROSSCHECK BACKUP;
  DELETE NOPROMPT OBSOLETE;
}
```
![8](6.png)
### 总结
归档日志模式是一种记录数据库日志的方式，它会在日志文件达到一定大小或者一定时间间隔后，将该日志文件压缩并归档存储。这种模式可以保留历史日志并且可以使用归档的日志文件进行恢复操作，同时也能有效减少硬盘空间占用。在使用该模式时需要注意设置相关参数，如归档间隔时间、归档文件的数量等
RMAN是Oracle提供的强大备份和恢复工具。通过编写备份脚本，我们能够自动化备份过程，并配置各种选项来满足特定需求。RMAN的灵活性和高效性使得备份过程更加可靠和高效。
设计一个完善的系统的数据库时，要保证设计的方案能够实现数据的安全性和可恢复性，才能保证一个系统的正常运行和操作，对于数据库设计来说，安全性和可维护性尤其重要。