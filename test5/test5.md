班级：软工2班
学号：202010414207
姓名：李沛云
## 实验5：包，过程，函数的用法

## 实验目的
-了解PL/SQL语言结构
-了解PL/SQL变量和常量的声明和使用方法
-学习包，过程，函数的用法。
## 实验内容
-以hr用户登录
1. 创建一个包(Package)，包名是MyPack。
``` mysql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
```
![1](mypack.png)
![2](cj.png)
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
``` mysql
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;
```
测试函数Get_SalaryAmount()
![3](cs1.png)
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID
``` mysql
PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
```
测试函数Get_Employees()
![4](cs2.png)
4. 在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
![5](cx.png)
```mysql
     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         1         101 Neena                      100
         2         108 Nancy                       101
         3         109 Daniel                       108
         3         113 Luis                        108
         3         112 Jose Manuel                 108
         3         111 Ismael                      108
         3         110 John                        108
         2         205 Shelley                      101
         3         206 William                      205
         2         204 Hermann                    101
         2         203 Susan                       101

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         200 Jennifer                     101
         1         201 Michael                      100
         2         202 Pat                         201
         1         149 Eleni                        100
         2         174 Ellen                        149
         2         179 Charles                     149
         2         178 Kimberely                    149
         2         177 Jack                        149
         2         176 Jonathon                    149
         2         175 Alyssa                      149
         1         148 Gerald                      100

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         168 Lisa                        148
         2         173 Sundita                      148
         2         172 Elizabeth                    148
         2         171 William                      148
         2         170 Tayler                      148
         2         169 Harrison                     148
         1         147 Alberto                      100
         2         162 Clara                       147
         2         167 Amit                        147
         2         166 Sundar                      147
         2         165 David                       147

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         164 Mattea                      147
         2         163 Danielle                     147
         1         146 Karen                       100
         2         156 Janette                      146
         2         161 Sarath                      146
         2         160 Louise                      146
         2         159 Lindsey                     146
         2         158 Allan                        146
         2         157 Patrick                      146
         1         145 John                        100
         2         150 Peter                       145

    
     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         138 Stephen                     123
         1         122 Payam                      100
         2         133 Jason                       122
         2         191 Randall                      122
         2         190 Timothy                     122
         2         189 Jennifer                     122
         2         188 Kelly                        122
         2         136 Hazel                       122
         2         135 Ki                          122
         2         134 Michael                      122
         1         121 Adam                       100

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         129 Laura                       121
         2         187 Anthony                     121
         2         186 Julia                        121
         2         185 Alexis                       121
         2         184 Nandita                     121
         2         132 TJ                          121
         2         131 James                       121
         2         130 Mozhe                      121
         1         120 Matthew                     100
         2         125 Julia                        120
         2         183 Girard                       120

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         182 Martha                      120
         2         181 Jean                        120
         2         180 Winston                     120
         2         128 Steven                      120
         2         127 James                       120
         2         126 Irene                       120
         1         114 Den                        100
         2         115 Alexander                   114
         2         119 Karen                       114
         2         118 Guy                        114
         2         117 Sigal                        114

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         116 Shelli                        114
         1         102 Lex                         100
         2         103 Alexander                   102
         3         104 Bruce                       103
         3         107 Diana                       103
         3         106 Valli                        103
         3         105 David                       103
         1         108 Nancy                       101
         2         109 Daniel                       108
         2         113 Luis                        108
         2         112 Jose Manuel                 108

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         2         111 Ismael                      108
         2         110 John                        108
         1         205 Shelley                      101
         2         206 William                      205
         1         204 Hermann                    101
         1         203 Susan                       101
         1         200 Jennifer                     101
         1         103 Alexander                   102
         2         104 Bruce                       103
         2         107 Diana                       103
         2         106 Valli                        103
     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         3         129 Laura                       121
         3         187 Anthony                     121
         3         186 Julia                        121
         3         185 Alexis                       121
         3         184 Nandita                     121
         3         132 TJ                          121
         3         131 James                       121
         3         130 Mozhe                      121
         2         120 Matthew                     100
         3         125 Julia                        120
         3         183 Girard                       120

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         3         182 Martha                      120
         3         181 Jean                        120
         3         180 Winston                     120
         3         128 Steven                      120
         3         127 James                       120
         3         126 Irene                       120
         2         114 Den                        100
         3         115 Alexander                   114
         3         119 Karen                       114
         3         118 Guy                        114
         3         117 Sigal                        114

     LEVEL EMPLOYEE_ID FIRST_NAME           MANAGER_ID
---------- ----------- -------------------- ----------
         3         116 Shelli                        114
         2         102 Lex                         100
         3         103 Alexander                   102
         4         104 Bruce                       103
         4         107 Diana                       103
         4         106 Valli                        103
         4         105 David                       103
已选择 315 行。
```